package com.ruoyi.project.sfgl_lgl.service;

import com.ruoyi.project.sfgl_lgl.domain.SfTable;

import java.util.List;

public interface SearchService {

    /**
     * 根据挂号单ID或患者姓名或订单状态进行查询
     * @param sfTable
     * @return
     */
    public List<SfTable> selSfTableInfo(SfTable sfTable);

    /**
     * 退费操作
     * @param ddh
     * @return
     */
    int upSfTable(String ddh);
}
