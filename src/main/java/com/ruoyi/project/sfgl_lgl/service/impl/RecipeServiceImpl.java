package com.ruoyi.project.sfgl_lgl.service.impl;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.ruoyi.project.sfgl_lgl.domain.*;
import com.ruoyi.project.sfgl_lgl.mapper.CheckPrescriptionMapper;
import com.ruoyi.project.sfgl_lgl.mapper.HuanZheMapper;
import com.ruoyi.project.sfgl_lgl.mapper.MedicationPrescriptionFormMapper;
import com.ruoyi.project.sfgl_lgl.mapper.SfTableMapper;
import com.ruoyi.project.sfgl_lgl.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {
    @Autowired
    private HuanZheMapper huanZheMapper;
    @Autowired
    private CheckPrescriptionMapper checkPrescriptionMapper;
    @Autowired
    private MedicationPrescriptionFormMapper medicationPrescriptionFormMapper;
    @Autowired
    private SfTableMapper sfTableMapper;

    /**
     * 通过挂号单ID查询患者信息
     * @param gh_id
     * @return
     */
    @Override
    public HuanZhe selHuanZheInfoByGhId(Integer gh_id) {
        return huanZheMapper.selHuanZheInfoByGhId(gh_id);
    }

    /**
     * 通过挂号单ID查询用药处方表
     * @param gh_id
     * @return
     */
    @Override
    public List<MedicationPrescriptionForm> selMedicationPrescriptionForm(Integer gh_id) {
        return medicationPrescriptionFormMapper.selMedicationPrescriptionForm(gh_id);
    }

    /**
     * 根据挂号单ID查询检查处方
     * @param gh_id
     * @return
     */
    @Override
    public List<CheckPrescription> selCheckPrescription(Integer gh_id){
        return checkPrescriptionMapper.selCheckPrescription(gh_id);
    };
    /**
     * 生成订单，修改处方收费表和药品收费表信息
     * @param queryParam
     */
    @Override
    public int makeOrders(QueryParam queryParam) {
        int n = 0;
        int gh_id = queryParam.getGh_id();
        //1.循环遍历检查处方ID，并通过挂号单ID，修改支付状态为“0”：支付成功
        String[] check_ids = queryParam.getCheck_ids().split(",");
        for(int i=0;i<check_ids.length;i++){
            n+=checkPrescriptionMapper.upStatus('0',gh_id,Integer.parseInt(check_ids[i]));
        }
        //2.循环遍历药品ID，并通过挂号单ID，修改支付状态为“0”：支付成功
        String[] medica_ids = queryParam.getMedica_ids().split(",");
        for(String medica_id:medica_ids){
           n+=medicationPrescriptionFormMapper.upStatus('0', gh_id,Integer.parseInt(medica_id) );
        }

        String dd_number = "DD"+System.currentTimeMillis();
        //查询患者姓名
        String hz_name = huanZheMapper.selHuanZhe(gh_id);
        //3.生成订单
        SfTable tabel = new SfTable(0,dd_number,gh_id,hz_name,queryParam.getSum_money(),"现金","支付成功",queryParam.getCheck_ids(),queryParam.getMedica_ids());
        n+=sfTableMapper.insInfo(tabel);

        return n;

    }

}
