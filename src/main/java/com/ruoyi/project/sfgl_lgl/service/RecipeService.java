package com.ruoyi.project.sfgl_lgl.service;

import com.ruoyi.project.sfgl_lgl.domain.*;

import java.util.List;

/**
 * 处方收费
 */
public interface RecipeService {
    //通过挂号单查询患者信息
    public HuanZhe selHuanZheInfoByGhId(Integer gh_id);

    /**
     * 根据挂号单ID查询检查处方
     * @param gh_id
     * @return
     */
    public List<CheckPrescription> selCheckPrescription(Integer gh_id);

    /**
     * 通过挂号单ID查询用药处方表
     * @param gh_id
     * @return
     */
    public List<MedicationPrescriptionForm> selMedicationPrescriptionForm(Integer gh_id);

    /**
     * 生成订单，修改处方收费表和药品收费表信息
     * @param queryParam
     */
    int makeOrders(QueryParam queryParam);


}
