package com.ruoyi.project.sfgl_lgl.service.impl;

import com.ruoyi.project.sfgl_lgl.domain.CheckPrescription;
import com.ruoyi.project.sfgl_lgl.domain.SfTable;
import com.ruoyi.project.sfgl_lgl.mapper.CheckPrescriptionMapper;
import com.ruoyi.project.sfgl_lgl.mapper.MedicationPrescriptionFormMapper;
import com.ruoyi.project.sfgl_lgl.mapper.SfTableMapper;
import com.ruoyi.project.sfgl_lgl.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private SfTableMapper sfTableMapper;
    @Autowired
    private CheckPrescriptionMapper checkPrescriptionMapper;
    @Autowired
    private MedicationPrescriptionFormMapper medicationPrescriptionFormMapper;


    /**
     * 根据挂号单ID或患者姓名或订单状态进行查询
     * @param sfTable
     * @return
     */
    @Override
    public List<SfTable> selSfTableInfo(SfTable sfTable) {
        return sfTableMapper.selSfTableInfo(sfTable);
    }

    /**
     * 退费操作
     * @param ddh
     * @return
     */
    @Override
    public int upSfTable(String ddh) {
        int n = 0;
        String status = "已退费";
        Character zhuangtai = '2';
        //1.获取订单对象(包含挂号单号,检查处方ids,用药处方ids)
        SfTable sfTable = sfTableMapper.selSfTablecheck_ids(ddh);
        //2.修改检查处方表状态
            //2.1获取检查处方表id
        String[] check_idArr = sfTable.getCheck_ids().split(",");
        for(String check_id : check_idArr){
            n+=checkPrescriptionMapper.upStatus(zhuangtai,sfTable.getGhdh(),Integer.parseInt(check_id));
        }
        //3.修改用药处方表状态
        String[] medica_ids = sfTable.getMedica_ids().split(",");
        for(String medica_id : medica_ids){
            n+=medicationPrescriptionFormMapper.upStatus(zhuangtai,sfTable.getGhdh(),Integer.parseInt(medica_id));
        }
        //4.修改订单表状态
        n+=sfTableMapper.upSfTableStatus(ddh,status);
        return n;
    }
}
