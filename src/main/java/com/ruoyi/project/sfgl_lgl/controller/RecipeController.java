package com.ruoyi.project.sfgl_lgl.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.sfgl_lgl.domain.CheckPrescription;
import com.ruoyi.project.sfgl_lgl.domain.HuanZhe;
import com.ruoyi.project.sfgl_lgl.domain.MedicationPrescriptionForm;
import com.ruoyi.project.sfgl_lgl.domain.QueryParam;
import com.ruoyi.project.sfgl_lgl.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处方收费操作处理
 */
@RestController
@RequestMapping("/sfgl_lgl/recipe")
public class RecipeController extends BaseController {
    @Autowired
    private RecipeService recipeService;

    /**
     * 获取处方收费页面
     */
    @PreAuthorize("@ss.hasPermi('sfgl_lgl:recipe:list')")
    @GetMapping("/list")
    public void list(){

    }

    @PreAuthorize("@ss.hasPermi('sfgl_lgl:recipe:search ')")
    @GetMapping("/search")
    public AjaxResult search(Integer gh_id){
        HuanZhe huanZhe = recipeService.selHuanZheInfoByGhId(gh_id);
        if(huanZhe==null){
            return AjaxResult.error("请输入正确的挂号单ID查询");
        }
        Map<String,Object> map = new HashMap<>();
        List<CheckPrescription> check = recipeService.selCheckPrescription(gh_id);
        List<MedicationPrescriptionForm> medica = recipeService.selMedicationPrescriptionForm(gh_id);
        map.put("huanzhe",huanZhe);
        map.put("check",check);
        map.put("medica",medica);
        return AjaxResult.success(map);
    }

    /**
     * 默认现金收费，并生成订单
     */
    @PreAuthorize("@ss.hasPermi('sfgl_lgl:recipe:pay')")
    @PostMapping("/pay")
    public AjaxResult pay(QueryParam queryParam){
        int n = recipeService.makeOrders(queryParam);
        if(n>0){
            return AjaxResult.success("成功生成订单");
        }else{
            return AjaxResult.error("操作失败");
        }

    }
}
