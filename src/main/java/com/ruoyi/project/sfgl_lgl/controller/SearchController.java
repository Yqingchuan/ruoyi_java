package com.ruoyi.project.sfgl_lgl.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.sfgl_lgl.domain.SfTable;
import com.ruoyi.project.sfgl_lgl.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sfgl_lgl/search")
public class SearchController extends BaseController {
    @Autowired
    private SearchService searchService;
    /**
     * 获取处方收费页面
     */
    @PreAuthorize("@ss.hasPermi('sfgl_lgl:search:list')")
    @GetMapping("/index")
    public TableDataInfo list(SfTable post)
    {
        startPage();
        List<SfTable> list = searchService.selSfTableInfo(post);
        return getDataTable(list);
    }
    /**
     * 退费
     */
    @PreAuthorize("@ss.hasPermi('sfgl_lgl:search:refund')")
    @GetMapping("/refund")
    public AjaxResult refund(String ddh)
    {
        int n = searchService.upSfTable(ddh);
        if(n>0){
            return AjaxResult.success("退费成功");
        }else{
            return AjaxResult.error("退费失败");
        }

    }
}
