package com.ruoyi.project.sfgl_lgl.mapper;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.ruoyi.project.sfgl_lgl.domain.CheckPrescription;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 检查处方
 */
public interface CheckPrescriptionMapper {
    /**
     * 根据挂号单ID查询检查处方
     * @param gh_id
     * @return
     */
    public List<CheckPrescription> selCheckPrescription(Integer gh_id);

    /**
     * 修改检查处方支付状态： 0：支付，1：未支付，2：退费
     * @param status
     * @param gh_id
     * @param check_id
     * @return
     */
    @Update("update check_prescription set otc_status = #{param1},update_time=sysdate() where refund_register_id = #{param2} and check_prescription=#{param3}")
    public int upStatus(Character status,Integer gh_id, Integer check_id);
}
