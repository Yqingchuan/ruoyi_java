package com.ruoyi.project.sfgl_lgl.mapper;

import com.ruoyi.project.sfgl_lgl.domain.CheckPrescription;
import com.ruoyi.project.sfgl_lgl.domain.SfTable;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SfTableMapper {
    /**
     * 生成订单
     * @param sfTable
     * @return
     */

    public int insInfo(SfTable sfTable);

    /**
     * 根据挂号单ID或患者姓名或订单状态进行查询
     * @param sfTable
     * @return
     */
    public List<SfTable> selSfTableInfo(SfTable sfTable);

    /**
     * 根据订单号修改订单支付状态:未支付,支付成功,已退费
     * @param ddh
     * @param status
     * @return
     */
    @Update("update sf_table set status = #{status},update_time=sysdate() where ddh=#{ddh}")
    int upSfTableStatus(@Param("ddh") String ddh,@Param("status") String status);

    /**
     * 根据订单号查询检查处方id
     * @param ddh
     * @return
     */
    @Select("select * from sf_table where ddh = #{0} ")
    SfTable selSfTablecheck_ids(String ddh);
}
