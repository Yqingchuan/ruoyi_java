package com.ruoyi.project.sfgl_lgl.mapper;

import com.ruoyi.project.sfgl_lgl.domain.HuanZhe;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface HuanZheMapper {
    public HuanZhe selHuanZheInfoByGhId(Integer gh_id);

    /**
     * 通过挂号单ID查询患者姓名
     * @param gh_id
     * @return
     */
    @Select("select pati_name from his_register where reg_id = #{reg_id}")
    String selHuanZhe(@Param("reg_id") int gh_id);
}
