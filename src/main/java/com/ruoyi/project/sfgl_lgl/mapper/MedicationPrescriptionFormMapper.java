package com.ruoyi.project.sfgl_lgl.mapper;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.ruoyi.project.sfgl_lgl.domain.MedicationPrescriptionForm;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 用药处方表
 */
public interface MedicationPrescriptionFormMapper {
    /**
     * 通过挂号单ID查询用药处方表
     * @param gh_id
     * @return
     */
    public List<MedicationPrescriptionForm> selMedicationPrescriptionForm(Integer gh_id);

    /**
     * 修改用药处方支付状态： 0：支付，1：未支付，2：退费
     * @param status
     * @param gh_id
     * @param check_id
     * @return
     */
    @Update("update medication_prescription_form set otc_status = #{param1},update_time=sysdate() where refund_register_id = #{param2} and drug_id =#{param3}")
    public int upStatus(Character status, Integer gh_id, Integer check_id);



}
