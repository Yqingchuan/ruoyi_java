package com.ruoyi.project.sfgl_lgl.domain;

import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用药处方表,一对多,一个挂号ID对应多个药
 */
public class MedicationPrescriptionForm extends BaseEntity {
    /**
     * 自增ID
     */
    private Integer id;
    /**
     * 挂号单ID
     */
    private Integer refund_register_id;
    /**
     * 数量
     */
    private Integer quantity;
    /**
     * 金额
     */
    private Double amount_of_money;
    /**
     * 服用备注
     */
    private String remark;
    /**
     * 支付状态
     */
    private String otc_status;
    /**
     * 药品信息
     */
    private DrugsDurg drugsDurg;

    @Override
    public String toString() {
        return "MedicationPrescriptionForm{" +
                "id=" + id +
                ", refund_register_id=" + refund_register_id +
                ", quantity=" + quantity +
                ", amount_of_money=" + amount_of_money +
                ", remark='" + remark + '\'' +
                ", otc_status='" + otc_status + '\'' +
                ", drugsDurg=" + drugsDurg +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefund_register_id() {
        return refund_register_id;
    }

    public void setRefund_register_id(Integer refund_register_id) {
        this.refund_register_id = refund_register_id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getAmount_of_money() {
        return amount_of_money;
    }

    public void setAmount_of_money(Double amount_of_money) {
        this.amount_of_money = amount_of_money;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOtc_status() {
        return otc_status;
    }

    public void setOtc_status(String otc_status) {
        this.otc_status = otc_status;
    }

    public DrugsDurg getDrugsDurg() {
        return drugsDurg;
    }

    public void setDrugsDurg(DrugsDurg drugsDurg) {
        this.drugsDurg = drugsDurg;
    }

    public MedicationPrescriptionForm(Integer id, Integer refund_register_id, Integer quantity, Double amount_of_money, String remark, String otc_status, DrugsDurg drugsDurg) {
        this.id = id;
        this.refund_register_id = refund_register_id;
        this.quantity = quantity;
        this.amount_of_money = amount_of_money;
        this.remark = remark;
        this.otc_status = otc_status;
        this.drugsDurg = drugsDurg;
    }

    public MedicationPrescriptionForm() {
    }
}
