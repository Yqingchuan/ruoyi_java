package com.ruoyi.project.sfgl_lgl.domain;

/**
 * 药品表
 */
public class DrugsDurg {
    /**
     * 药品ID
     */
    private Integer drug_id;
    /**
     * 药品名称
     */
    private String drug_name;
    /**
     * 药品单价
     */
    private Double drug_otc_price;

    @Override
    public String toString() {
        return "DrugsDurg{" +
                "drug_id=" + drug_id +
                ", drug_name='" + drug_name + '\'' +
                ", drug_otc_price=" + drug_otc_price +
                '}';
    }

    public Integer getDrug_id() {
        return drug_id;
    }

    public void setDrug_id(Integer drug_id) {
        this.drug_id = drug_id;
    }

    public String getDrug_name() {
        return drug_name;
    }

    public void setDrug_name(String drug_name) {
        this.drug_name = drug_name;
    }

    public Double getDrug_otc_price() {
        return drug_otc_price;
    }

    public void setDrug_otc_price(Double drug_otc_price) {
        this.drug_otc_price = drug_otc_price;
    }

    public DrugsDurg(Integer drug_id, String drug_name, Double drug_otc_price) {
        this.drug_id = drug_id;
        this.drug_name = drug_name;
        this.drug_otc_price = drug_otc_price;
    }

    public DrugsDurg() {
    }
}
