package com.ruoyi.project.sfgl_lgl.domain;

/**
 * 检查费用表
 */
public class SystemProjectFee {
    /**
     * 检查项目ID
     */
    private Integer project_id;
    /**
     * 项目名称
     */
    private String project_name;
    /**
     * 单位
     */
    private String project_times;

    /**
     * 项目单价
     */
    private Double project_unitprice;

    @Override
    public String toString() {
        return "SystemProjectFee{" +
                "project_id=" + project_id +
                ", project_name='" + project_name + '\'' +
                ", project_times='" + project_times + '\'' +
                ", project_unitprice=" + project_unitprice +
                '}';
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_times() {
        return project_times;
    }

    public void setProject_times(String project_times) {
        this.project_times = project_times;
    }

    public Double getProject_unitprice() {
        return project_unitprice;
    }

    public void setProject_unitprice(Double project_unitprice) {
        this.project_unitprice = project_unitprice;
    }

    public SystemProjectFee() {
    }

    public SystemProjectFee(Integer project_id, String project_name, String project_times, Double project_unitprice) {
        this.project_id = project_id;
        this.project_name = project_name;
        this.project_times = project_times;
        this.project_unitprice = project_unitprice;
    }
}
