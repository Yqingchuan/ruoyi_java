package com.ruoyi.project.sfgl_lgl.domain;

import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 收费单
 */
public class SfTable extends BaseEntity {
    /**
     * 主键ID
     */
    private Integer id;
    /**
     * 订单号
     */
    private String ddh;
    /**
     * 挂号单号
     */
    private Integer ghdh;
    /**
     * 患者姓名
     */
    private String hz_name;
    /**
     * 总金额
     */
    private Double z_money;
    /**
     * 支付类型
     */
    private String zf_type;
    /**
     * 订单状态
     */
    private String status;
    /**
     * 检查处方ids
     */
    private String check_ids;
    /**
     * 用药处方ids
     */
    private String medica_ids;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDdh() {
        return ddh;
    }

    public void setDdh(String ddh) {
        this.ddh = ddh;
    }

    public Integer getGhdh() {
        return ghdh;
    }

    public void setGhdh(Integer ghdh) {
        this.ghdh = ghdh;
    }

    public String getHz_name() {
        return hz_name;
    }

    public void setHz_name(String hz_name) {
        this.hz_name = hz_name;
    }

    public Double getZ_money() {
        return z_money;
    }

    public void setZ_money(Double z_money) {
        this.z_money = z_money;
    }

    public String getZf_type() {
        return zf_type;
    }

    public void setZf_type(String zf_type) {
        this.zf_type = zf_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SfTable(Integer id, String ddh, Integer ghdh, String hz_name, Double z_money, String zf_type, String status, String check_ids, String medica_ids) {
        this.id = id;
        this.ddh = ddh;
        this.ghdh = ghdh;
        this.hz_name = hz_name;
        this.z_money = z_money;
        this.zf_type = zf_type;
        this.status = status;
        this.check_ids = check_ids;
        this.medica_ids = medica_ids;
    }

    @Override
    public String toString() {
        return "SfTable{" +
                "id=" + id +
                ", ddh='" + ddh + '\'' +
                ", ghdh=" + ghdh +
                ", hz_name='" + hz_name + '\'' +
                ", z_money=" + z_money +
                ", zf_type='" + zf_type + '\'' +
                ", status='" + status + '\'' +
                ", check_ids='" + check_ids + '\'' +
                ", medica_ids='" + medica_ids + '\'' +
                '}';
    }

    public String getCheck_ids() {
        return check_ids;
    }

    public void setCheck_ids(String check_ids) {
        this.check_ids = check_ids;
    }

    public String getMedica_ids() {
        return medica_ids;
    }

    public void setMedica_ids(String medica_ids) {
        this.medica_ids = medica_ids;
    }

    public SfTable() {
    }
}
