package com.ruoyi.project.sfgl_lgl.domain;

import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 检查处方表  内嵌   检查费用表
 */
public class CheckPrescription extends BaseEntity {
    /**
     * 主键ID
     */
    private Integer id;
    /**
     * 挂号单ID
     */
    private Integer refund_register_id;
    /**
     * 检查备注
     */
    private String check_remark;
    /**
     * 支付状态(0:支付,1:未支付)
     */
    private String otc_status;
    /**
     * 检查项目编号
     */
    private Integer check_prescription;
    /**
     * 检查项目
     */
    private SystemProjectFee systemProjectFee;

    @Override
    public String toString() {
        return "CheckPrescription{" +
                "id=" + id +
                ", refund_register_id=" + refund_register_id +
                ", check_remark='" + check_remark + '\'' +
                ", otc_status='" + otc_status + '\'' +
                ", check_prescription=" + check_prescription +
                ", systemProjectFee=" + systemProjectFee +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRefund_register_id() {
        return refund_register_id;
    }

    public void setRefund_register_id(Integer refund_register_id) {
        this.refund_register_id = refund_register_id;
    }

    public String getCheck_remark() {
        return check_remark;
    }

    public void setCheck_remark(String check_remark) {
        this.check_remark = check_remark;
    }

    public String getOtc_status() {
        return otc_status;
    }

    public void setOtc_status(String otc_status) {
        this.otc_status = otc_status;
    }

    public Integer getCheck_prescription() {
        return check_prescription;
    }

    public void setCheck_prescription(Integer check_prescription) {
        this.check_prescription = check_prescription;
    }

    public SystemProjectFee getSystemProjectFee() {
        return systemProjectFee;
    }

    public void setSystemProjectFee(SystemProjectFee systemProjectFee) {
        this.systemProjectFee = systemProjectFee;
    }

    public CheckPrescription() {
    }

    public CheckPrescription(Integer id, Integer refund_register_id, String check_remark, String otc_status, Integer check_prescription, SystemProjectFee systemProjectFee) {
        this.id = id;
        this.refund_register_id = refund_register_id;
        this.check_remark = check_remark;
        this.otc_status = otc_status;
        this.check_prescription = check_prescription;
        this.systemProjectFee = systemProjectFee;
    }
}
