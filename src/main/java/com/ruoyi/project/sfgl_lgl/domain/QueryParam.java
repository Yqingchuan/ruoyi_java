package com.ruoyi.project.sfgl_lgl.domain;

import java.util.Arrays;

public class QueryParam {
    private Integer gh_id;
    private String check_ids;
    private String medica_ids;
    private Double sum_money;

    @Override
    public String toString() {
        return "QueryParam{" +
                "gh_id=" + gh_id +
                ", check_ids='" + check_ids + '\'' +
                ", medica_ids='" + medica_ids + '\'' +
                ", sum_money=" + sum_money +
                '}';
    }

    public Integer getGh_id() {
        return gh_id;
    }

    public void setGh_id(Integer gh_id) {
        this.gh_id = gh_id;
    }

    public String getCheck_ids() {
        return check_ids;
    }

    public void setCheck_ids(String check_ids) {
        this.check_ids = check_ids;
    }

    public String getMedica_ids() {
        return medica_ids;
    }

    public void setMedica_ids(String medica_ids) {
        this.medica_ids = medica_ids;
    }

    public Double getSum_money() {
        return sum_money;
    }

    public void setSum_money(Double sum_money) {
        this.sum_money = sum_money;
    }

    public QueryParam(Integer gh_id, String check_ids, String medica_ids, Double sum_money) {
        this.gh_id = gh_id;
        this.check_ids = check_ids;
        this.medica_ids = medica_ids;
        this.sum_money = sum_money;
    }

    public QueryParam() {
    }
}
