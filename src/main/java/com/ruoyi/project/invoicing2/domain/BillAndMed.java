package com.ruoyi.project.invoicing2.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BillAndMed extends Bill{
    private Bill bill;
    private List<InMed> meds;
}
