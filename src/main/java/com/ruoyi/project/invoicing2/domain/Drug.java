package com.ruoyi.project.invoicing2.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Drug extends BaseEntity {
    /*药品id*/
    private  Integer drugId;
    /*药品名称*/
    private String drugName;
    /*药品编码*/
    private String drugCoding;
    /*工厂名称*/
    private Integer facId;
    /*关键字*/
    private String drugKey;
    /*药品类型*/
    private String drugType;
    /*处方类型*/
    private String drugPresType;
    /*单位*/
    private String unit;
    /*处方价格*/
    private double drugPresPrice;
    /*药品库存*/
    private int drugReserve;
    /*预警值*/
    private int warnValue;
    /*换算量*/
    private String conversion;
    /*状态*/
    private String status;


}
