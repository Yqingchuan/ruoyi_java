package com.ruoyi.project.invoicing2.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
//单据表
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Bill  extends BaseEntity {
    private Integer billId;//单据id
    private String billGy;//供应商
    private int billAllMoney;//批发总额
    private String status;//状态
    private String billIn;//是否出库
    private String billApply;//申请人
    private String billMake;//制单人
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date billInTime;//入库时间
}
