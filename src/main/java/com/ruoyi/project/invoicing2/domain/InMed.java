package com.ruoyi.project.invoicing2.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//进药表
@AllArgsConstructor
@NoArgsConstructor
@Data
public class InMed extends BaseEntity {
    private Integer drugId;//药品id
    private String drugName;//药品名称
    private String facName;//生产厂家
    private int medNum;//订购数量
    private String unit;//单位
    private int medPrice;//批发价
    private int medAll;//总批发额
    private String medPCH;//批次号
    private String drugType;//药品类型
    private Integer billId;//订单id
    private Drug drug;
    private Integer facId;
}
