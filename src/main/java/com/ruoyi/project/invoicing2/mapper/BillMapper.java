package com.ruoyi.project.invoicing2.mapper;

import com.ruoyi.project.invoicing2.domain.Bill;
import com.ruoyi.project.invoicing2.domain.InMed;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BillMapper {

    List<Bill> selectList(Bill bill);

    int addBill(Bill bill);


    int delete(Long[] billIds);

    int update(Long[] billIds);
    int update2(Long[] billIds);


    int updateNum(InMed inMed);
}
