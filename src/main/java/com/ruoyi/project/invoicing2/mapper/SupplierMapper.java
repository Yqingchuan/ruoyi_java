package com.ruoyi.project.invoicing2.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface SupplierMapper {
    @Select("select sup_name name from his_supplier")
    List<String> selectName();
}
