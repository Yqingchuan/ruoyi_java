package com.ruoyi.project.invoicing2.mapper;

import com.ruoyi.project.invoicing2.domain.InMed;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MedMapper {
    int addMed(@Param("meds") List<InMed> meds);
}
