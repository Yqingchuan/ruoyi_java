package com.ruoyi.project.invoicing2.service.impl;

import com.ruoyi.project.invoicing2.mapper.SupplierMapper;
import com.ruoyi.project.invoicing2.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierMapper supplierMapper;
    @Override
    public List<String> selName() {
        return supplierMapper.selectName();
    }
}
