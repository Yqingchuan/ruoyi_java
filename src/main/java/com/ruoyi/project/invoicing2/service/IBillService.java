package com.ruoyi.project.invoicing2.service;

import com.ruoyi.project.invoicing2.domain.Bill;
import com.ruoyi.project.invoicing2.domain.BillAndMed;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IBillService {
    List<Bill> selectBill(Bill bill);

    int addBill(BillAndMed billAndMed);

    int removeBills(@Param("billIds") Long[] billIds);

    int updateBills(@Param("billIds") Long[] billIds);
    int updateBills2(@Param("billIds") Long[] billIds);
}
