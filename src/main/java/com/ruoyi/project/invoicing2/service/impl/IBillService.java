package com.ruoyi.project.invoicing2.service.impl;

import com.ruoyi.project.invoicing2.domain.Bill;
import com.ruoyi.project.invoicing2.domain.BillAndMed;
import com.ruoyi.project.invoicing2.domain.InMed;
import com.ruoyi.project.invoicing2.mapper.BillMapper;
import com.ruoyi.project.invoicing2.mapper.MedMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service
public class IBillService implements com.ruoyi.project.invoicing2.service.IBillService {
    @Autowired
    private BillMapper billMapper;
    @Autowired
    private MedMapper medMapper;
    @Override
    public List<Bill> selectBill(Bill bill) {
        return billMapper.selectList(bill);
    }

    @Override
    public int removeBills(Long[] billIds) {
        return billMapper.delete(billIds);

    }

    @Override
    public int updateBills(Long[] billIds) {
        return billMapper.update(billIds);
    }

    @Override
    public int updateBills2(Long[] billIds) {
        return billMapper.update2(billIds);
    }

    @Override
    @Transactional
    public int addBill(BillAndMed billAndMed) {
        List<InMed> meds = billAndMed.getMeds();
        Bill bill = billAndMed.getBill();
        for (InMed m:meds){
            m.setBillId(bill.getBillId());
            m.setCreateTime(new Date());
        }
        int i = medMapper.addMed(meds);
        System.out.println(i);
        bill.setCreateTime(new Date());
        bill.setStatus("未审核");
        bill.setBillIn("未入库");
        bill.setBillApply(bill.getBillMake());
        bill.setBillInTime(new Date());
        int i1 = billMapper.addBill(bill);
        System.out.println(i1);
        int n=0;
        if (i>0 & i1>0){
            for (InMed m:meds){
                n=billMapper.updateNum(m);
            }
            return n;
        }
        return 0;
    }
}
