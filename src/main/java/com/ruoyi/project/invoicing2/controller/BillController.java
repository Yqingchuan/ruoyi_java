package com.ruoyi.project.invoicing2.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.invoicing2.domain.Bill;
import com.ruoyi.project.invoicing2.domain.BillAndMed;
import com.ruoyi.project.invoicing2.service.IBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/system/drug")
@RestController
public class BillController extends BaseController {
    @Autowired
    private IBillService iBillService;

    @GetMapping("/list")
    public TableDataInfo list(Bill bill){
        startPage();
        List<Bill> bills = iBillService.selectBill(bill);
        return getDataTable(bills);
    }

    @PostMapping
    public AjaxResult addBill(@RequestBody BillAndMed billAndMed){
        System.out.println(billAndMed);

        return toAjax(iBillService.addBill(billAndMed));
    }

    @DeleteMapping("/{billIds}")
    @PreAuthorize("@ss.hasAnyPermi('system:bill:remove')")
    public AjaxResult remove(@PathVariable Long[] billIds){
        return toAjax(iBillService.removeBills(billIds));
    }
    @PutMapping("/{billIds}")
    public AjaxResult update(@PathVariable Long[] billIds){
        return toAjax(iBillService.updateBills(billIds));
    }


}
