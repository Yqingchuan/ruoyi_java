package com.ruoyi.project.invoicing2.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.invoicing2.service.IBillService;
import com.ruoyi.project.invoicing2.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/system/supperlier")
public class SupperlierController extends BaseController {
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private IBillService iBillService;
    @RequestMapping("/name")
    public AjaxResult name(){
        return AjaxResult.success(supplierService.selName());
    }
    @PutMapping("/{billIds2}")
    public AjaxResult update2(@PathVariable Long[] billIds2){
        return toAjax(iBillService.updateBills2(billIds2));
    }
}
