package com.ruoyi.project.kbjz_lss.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

public class Register extends BaseEntity {
    private Integer regId;
    private String patiCode;
    private String patiName;
    private String regDepts;
    private Integer regDeid;
    private String regDocter;
    private String regPrice;
    private Integer regNum;
    private String regStatus;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date regSdate;
    private String regType;
    private String regTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date regRdate;


    @Override
    public String toString() {
        return "Register{" +
                "regId=" + regId +
                ", patiCode='" + patiCode + '\'' +
                ", patiName='" + patiName + '\'' +
                ", regDepts='" + regDepts + '\'' +
                ", regDeid=" + regDeid +
                ", regDocter='" + regDocter + '\'' +
                ", regPrice='" + regPrice + '\'' +
                ", regNum=" + regNum +
                ", regStatus='" + regStatus + '\'' +
                ", regSdate=" + regSdate +
                ", regType='" + regType + '\'' +
                ", regTime='" + regTime + '\'' +
                ", regRdate=" + regRdate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Register register = (Register) o;
        return Objects.equals(regId, register.regId) &&
                Objects.equals(patiCode, register.patiCode) &&
                Objects.equals(patiName, register.patiName) &&
                Objects.equals(regDepts, register.regDepts) &&
                Objects.equals(regDeid, register.regDeid) &&
                Objects.equals(regDocter, register.regDocter) &&
                Objects.equals(regPrice, register.regPrice) &&
                Objects.equals(regNum, register.regNum) &&
                Objects.equals(regStatus, register.regStatus) &&
                Objects.equals(regSdate, register.regSdate) &&
                Objects.equals(regType, register.regType) &&
                Objects.equals(regTime, register.regTime) &&
                Objects.equals(regRdate, register.regRdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regId, patiCode, patiName, regDepts, regDeid, regDocter, regPrice, regNum, regStatus, regSdate, regType, regTime, regRdate);
    }

    public Integer getRegId() {
        return regId;
    }

    public void setRegId(Integer regId) {
        this.regId = regId;
    }

    public String getPatiCode() {
        return patiCode;
    }

    public void setPatiCode(String patiCode) {
        this.patiCode = patiCode;
    }

    public String getPatiName() {
        return patiName;
    }

    public void setPatiName(String patiName) {
        this.patiName = patiName;
    }

    public String getRegDepts() {
        return regDepts;
    }

    public void setRegDepts(String regDepts) {
        this.regDepts = regDepts;
    }

    public Integer getRegDeid() {
        return regDeid;
    }

    public void setRegDeid(Integer regDeid) {
        this.regDeid = regDeid;
    }

    public String getRegDocter() {
        return regDocter;
    }

    public void setRegDocter(String regDocter) {
        this.regDocter = regDocter;
    }

    public String getRegPrice() {
        return regPrice;
    }

    public void setRegPrice(String regPrice) {
        this.regPrice = regPrice;
    }

    public Integer getRegNum() {
        return regNum;
    }

    public void setRegNum(Integer regNum) {
        this.regNum = regNum;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public Date getRegSdate() {
        return regSdate;
    }

    public void setRegSdate(Date regSdate) {
        this.regSdate = regSdate;
    }

    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public Date getRegRdate() {
        return regRdate;
    }

    public void setRegRdate(Date regRdate) {
        this.regRdate = regRdate;
    }

    public Register(Integer regId, String patiCode, String patiName, String regDepts, Integer regDeid, String regDocter, String regPrice, Integer regNum, String regStatus, Date regSdate, String regType, String regTime, Date regRdate) {
        this.regId = regId;
        this.patiCode = patiCode;
        this.patiName = patiName;
        this.regDepts = regDepts;
        this.regDeid = regDeid;
        this.regDocter = regDocter;
        this.regPrice = regPrice;
        this.regNum = regNum;
        this.regStatus = regStatus;
        this.regSdate = regSdate;
        this.regType = regType;
        this.regTime = regTime;
        this.regRdate = regRdate;
    }

    public Register() {
    }
}
