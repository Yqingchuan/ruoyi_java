package com.ruoyi.project.kbjz_lss.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

public class Patient extends BaseEntity {
    private Integer patiId;
    private String patiName;
    private String patiCode;
    private String patiPhone;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date patiBirthday;
    private String patiAge;
    private String patiSex;
    private String patiAddress;
    private String patiAllergy;
    private String status;

    @Override
    public String toString() {
        return "Patient{" +
                "patiId=" + patiId +
                ", patiName='" + patiName + '\'' +
                ", patiCode='" + patiCode + '\'' +
                ", patiPhone='" + patiPhone + '\'' +
                ", patiBirthday=" + patiBirthday +
                ", patiAge='" + patiAge + '\'' +
                ", patiSex='" + patiSex + '\'' +
                ", patiAddress='" + patiAddress + '\'' +
                ", patiAllergy='" + patiAllergy + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(patiId, patient.patiId) &&
                Objects.equals(patiName, patient.patiName) &&
                Objects.equals(patiCode, patient.patiCode) &&
                Objects.equals(patiPhone, patient.patiPhone) &&
                Objects.equals(patiBirthday, patient.patiBirthday) &&
                Objects.equals(patiAge, patient.patiAge) &&
                Objects.equals(patiSex, patient.patiSex) &&
                Objects.equals(patiAddress, patient.patiAddress) &&
                Objects.equals(patiAllergy, patient.patiAllergy) &&
                Objects.equals(status, patient.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patiId, patiName, patiCode, patiPhone, patiBirthday, patiAge, patiSex, patiAddress, patiAllergy, status);
    }

    public Integer getPatiId() {
        return patiId;
    }

    public void setPatiId(Integer patiId) {
        this.patiId = patiId;
    }

    public String getPatiName() {
        return patiName;
    }

    public void setPatiName(String patiName) {
        this.patiName = patiName;
    }

    public String getPatiCode() {
        return patiCode;
    }

    public void setPatiCode(String patiCode) {
        this.patiCode = patiCode;
    }

    public String getPatiPhone() {
        return patiPhone;
    }

    public void setPatiPhone(String patiPhone) {
        this.patiPhone = patiPhone;
    }

    public Date getPatiBirthday() {
        return patiBirthday;
    }

    public void setPatiBirthday(Date patiBirthday) {
        this.patiBirthday = patiBirthday;
    }

    public String getPatiAge() {
        return patiAge;
    }

    public void setPatiAge(String patiAge) {
        this.patiAge = patiAge;
    }

    public String getPatiSex() {
        return patiSex;
    }

    public void setPatiSex(String patiSex) {
        this.patiSex = patiSex;
    }

    public String getPatiAddress() {
        return patiAddress;
    }

    public void setPatiAddress(String patiAddress) {
        this.patiAddress = patiAddress;
    }

    public String getPatiAllergy() {
        return patiAllergy;
    }

    public void setPatiAllergy(String patiAllergy) {
        this.patiAllergy = patiAllergy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Patient(Integer patiId, String patiName, String patiCode, String patiPhone, Date patiBirthday, String patiAge, String patiSex, String patiAddress, String patiAllergy, String status) {
        this.patiId = patiId;
        this.patiName = patiName;
        this.patiCode = patiCode;
        this.patiPhone = patiPhone;
        this.patiBirthday = patiBirthday;
        this.patiAge = patiAge;
        this.patiSex = patiSex;
        this.patiAddress = patiAddress;
        this.patiAllergy = patiAllergy;
        this.status = status;
    }

    public Patient() {
    }
}
