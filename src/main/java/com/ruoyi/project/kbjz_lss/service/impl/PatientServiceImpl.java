package com.ruoyi.project.kbjz_lss.service.impl;

import com.ruoyi.project.kbjz_lss.domain.Patient;
import com.ruoyi.project.kbjz_lss.mapper.PatientMapper;
import com.ruoyi.project.kbjz_lss.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {
    @Autowired
    private PatientMapper patientMapper;
//动态分页查询
    @Override
    public List<Patient> selectPatientList(Patient patient) {

        return patientMapper.selectPatientList(patient);
    }
    /**
     * 在门诊挂号，根据患者身份证号，查询患者信息
     *
     * @param patiCode 患者身份证号
     * @return 角色对象信息
     */
    @Override
    public Patient selectPatientById(String patiCode) {

        return patientMapper.selectPatientById(patiCode);
    }
    /**
     * 新增患者信息
     *
     * @param patient 患者信息
     * @return 结果
     */
    @Override
    public int insertPatient(Patient patient) {
        return patientMapper.insertPatient(patient);
    }
}
