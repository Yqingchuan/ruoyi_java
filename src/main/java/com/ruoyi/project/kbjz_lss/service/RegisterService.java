package com.ruoyi.project.kbjz_lss.service;

import com.ruoyi.project.kbjz_lss.domain.Register;
import com.ruoyi.project.system.domain.SysPost;

import java.util.List;

public interface RegisterService {
    /**
     * 查询挂号单
     *
     * @param register 岗位信息
     * @return 岗位数据集合
     */
    public List<Register> selectRegisterList(Register register);
    /**
     * 修改保存挂号单，患者状态
     *
     * @param register 挂号单
     * @return 结果
     */
    public int updateRegister(Register register);
    /**
     * 批量删除挂号单信息
     *
     * @param regIds 需要删除的挂号单ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deleteRegisterByIds(Integer[] regIds);
    /**
     * 新增保存挂号单
     *
     * @param register 挂号单
     * @return 结果
     */
    public int insertRegister(Register register);
}
