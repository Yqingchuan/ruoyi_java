package com.ruoyi.project.kbjz_lss.service.impl;

import com.ruoyi.project.kbjz_lss.domain.Register;
import com.ruoyi.project.kbjz_lss.mapper.RegisterMapper;
import com.ruoyi.project.kbjz_lss.service.RegisterService;
import com.ruoyi.project.system.domain.SysPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RegisterServiceImpl implements RegisterService {
    //声明属性
    @Autowired
    private RegisterMapper registerMapper;

    /**
     * 查询挂号单
     *
     * @param register 挂号单信息
     * @return 挂号单信息集合
     */
    @Override
    public List<Register> selectRegisterList(Register register) {

        return registerMapper.selectRegisterList(register);
    }
    /**
     * 修改保存挂号单，患者状态
     *
     * @param register 挂号单
     * @return 结果
     */
    @Override
    public int updateRegister(Register register) {
        return registerMapper.updateRegister(register);

    }
    /**
     * 批量删除挂号单信息
     *
     * @param regIds 需要删除的挂号单ID
     * @return 结果
     * @throws Exception 异常
     */
    @Override
    public int deleteRegisterByIds(Integer[] regIds) {
        return registerMapper.deleteRegisterByIds(regIds);
    }
    /**
     * 新增保存挂号单
     *
     * @param register 挂号单
     * @return 结果
     */
    @Override
    public int insertRegister(Register register) {
        return registerMapper.insertRegister(register);
    }
}
