package com.ruoyi.project.kbjz_lss.mapper;

import com.ruoyi.project.kbjz_lss.domain.Register;
import com.ruoyi.project.system.domain.SysPost;

import java.util.List;

public interface RegisterMapper {
    /**
     * 动态分页查询
     *
     * @param register 挂号表信息
     * @return 挂号表信息
     */
    public List<Register> selectRegisterList(Register register);
    /**
     * 修改保存挂号单，患者状态
     *
     * @param register 挂号单信息
     * @return 结果
     */
    public int updateRegister(Register register);
    /**
     * 批量删除挂号单信息
     *
     * @param regId 需要删除的挂号单ID
     * @return 结果
     */
    public int deleteRegisterByIds(Integer[] regId);
    /**
     * 新增挂号单
     *
     * @param register 挂号单
     * @return 结果
     */
    public int insertRegister(Register register);
}
