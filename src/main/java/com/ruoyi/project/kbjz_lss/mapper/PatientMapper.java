package com.ruoyi.project.kbjz_lss.mapper;

import com.ruoyi.project.kbjz_lss.domain.Patient;
import com.ruoyi.project.system.domain.SysPost;

import java.util.List;

public interface PatientMapper {
    //动态分页查询
    public List<Patient> selectPatientList(Patient patient);
    /**
     * 在门诊挂号，根据患者身份证号，查询患者信息
     *
     * @param patiCode 患者身份证号
     * @return 角色对象信息
     */
    public Patient selectPatientById(String patiCode);
    /**
     * 新增患者信息
     *
     * @param patient 患者信息
     * @return 结果
     */
    public int insertPatient(Patient patient);
}
