package com.ruoyi.project.kbjz_lss.controller;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.kbjz_lss.domain.Register;
import com.ruoyi.project.kbjz_lss.service.RegisterService;
import com.ruoyi.project.system.domain.SysPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/kbjz_lss/ghlb")
public class RegisterController extends BaseController {
    //声明业务层属性
    @Autowired
    private RegisterService registerService;
    /**
     * 获取岗位列表
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:ghlb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Register register) {
        startPage();
        List<Register> registers = registerService.selectRegisterList(register);
        return getDataTable(registers);
    }
    /**
     * 修改岗位
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:ghlb:edit')")
    @Log(title = "挂号单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Register register) {
        register.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(registerService.updateRegister(register));
    }
    /**
     * 批量删除挂号单信息
     *
     * @param regIds 需要删除的挂号单ID
     * @return 结果
     * @throws Exception 异常
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:ghlb:remove')")
    @Log(title = "挂号单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{regIds}")
    public AjaxResult remove(@PathVariable Integer[] regIds) {
        return toAjax(registerService.deleteRegisterByIds(regIds));

    }
    /**
     * 新增挂号单
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:ghlb:add')")
    @Log(title = "挂号单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Register register)
    {
        register.setCreateBy(SecurityUtils.getUsername());
        return toAjax(registerService.insertRegister(register));
    }

}
