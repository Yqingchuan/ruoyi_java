package com.ruoyi.project.kbjz_lss.controller;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.kbjz_lss.domain.Patient;
import com.ruoyi.project.kbjz_lss.service.PatientService;
import com.ruoyi.project.system.domain.SysPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/kbjz_lss/hzk")
public class PatientController extends BaseController {
    //声明业务层属性
    @Autowired
    private PatientService patientService;
    //动态分页查询
    @PreAuthorize("@ss.hasPermi('kbjz_lss:hzk:list')")
    @GetMapping("/list")
    public TableDataInfo list(Patient patient) {
        startPage();
        List<Patient> list = patientService.selectPatientList(patient);
        return getDataTable(list);
    }
    /**
     * 在门诊挂号，根据患者身份证号，查询患者信息
     *
     * @param patiCode 患者身份证号
     * @return 角色对象信息
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:mzgh:query')")
    @GetMapping(value = "/{patiCode}")
    public AjaxResult getInfo(@PathVariable String patiCode) {
        return AjaxResult.success(patientService.selectPatientById(patiCode));
    }

    /**
     * 新增患者信息
     */
    @PreAuthorize("@ss.hasPermi('kbjz_lss:mzgh:add')")
    @Log(title = "新增患者", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add( @RequestBody Patient patient) {
        patient.setCreateBy(SecurityUtils.getUsername());
        return toAjax(patientService.insertPatient(patient));
    }


}
