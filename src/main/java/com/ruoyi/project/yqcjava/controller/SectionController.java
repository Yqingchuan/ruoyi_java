package com.ruoyi.project.yqcjava.controller;


import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;

import com.ruoyi.project.system.domain.SysPost;
import com.ruoyi.project.yqcjava.domain.Section;
import com.ruoyi.project.yqcjava.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/section")
public class SectionController extends BaseController {

    @Autowired
    private SectionService  sectionService;


    //查询科室
    @PreAuthorize("@ss.hasPermi('system:section:list')")
    @GetMapping("/list")
    public TableDataInfo list(Section section){
        startPage();
        List<Section> list = sectionService.selectSection(section);
        return getDataTable(list);
    }

    //新增科室
    @PreAuthorize("@ss.hasPermi('system:section:add')")
    @Log(title = "科室管理", businessType = BusinessType.INSERT)
    @PostMapping("/list")
    public AjaxResult add( @RequestBody Section section)
    {

        section.setCreateBy(SecurityUtils.getUsername());
        return toAjax(sectionService.insection(section));
    }

    /**
     * 根据科室编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:section:query')")
    @GetMapping(value = "/{secId}")
    public AjaxResult getInfo(@PathVariable Long secId)
    {
        return AjaxResult.success(sectionService.selectById(secId));
    }

    //修改科室信息
    @PreAuthorize("@ss.hasPermi('system:system:edit')")
    @Log(title = "科室管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit( @RequestBody Section section)
    {

        section.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(sectionService.upSection(section));
    }

    //删除科室信息
    @PreAuthorize("@ss.hasPermi('system:section:remove')")
    @Log(title = "科室管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{secId}")
    public AjaxResult remove(@PathVariable Long[] secId)
    {
        return toAjax(sectionService.deleteSectionById(secId));
    }

}
