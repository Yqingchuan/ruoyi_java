package com.ruoyi.project.yqcjava.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.yqcjava.domain.Inspection;
import com.ruoyi.project.yqcjava.domain.Section;
import com.ruoyi.project.yqcjava.service.InspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/cost")
public class inspectionController extends BaseController {

    @Autowired
    private InspectionService  inspectionService;

    //查询科室
    @PreAuthorize("@ss.hasPermi('system:cost:list')")
    @GetMapping("/list")
    public TableDataInfo list(Inspection inspection){
        startPage();
        List<Inspection> list = inspectionService.selectInspection(inspection);
        return getDataTable(list);
    }

    //新增科室
    @PreAuthorize("@ss.hasPermi('system:cost:add')")
    @Log(title = "检查管理", businessType = BusinessType.INSERT)
    @PostMapping("/list")
    public AjaxResult add(@RequestBody Inspection inspection)
    {

        inspection.setCreateBy(SecurityUtils.getUsername());
        return toAjax(inspectionService.insection(inspection));
    }

    /**
     * 根据检查ID获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:cost:query')")
    @GetMapping(value = "/{projectId}")
    public AjaxResult getInfo(@PathVariable Long projectId)
    {
        return AjaxResult.success(inspectionService.selectById(projectId));
    }


    //修改检查费用信息
    @PreAuthorize("@ss.hasPermi('system:cost:edit')")
    @Log(title = "检查管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit( @RequestBody Inspection inspection)
    {

        inspection.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(inspectionService.updateInspec(inspection));
    }

    //删除检查费用信息
    @PreAuthorize("@ss.hasPermi('system:cost:remove')")
    @Log(title = "检查管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{projectId}")
    public AjaxResult remove(@PathVariable Long[] projectId)
    {
        return toAjax(inspectionService.deleInspec(projectId));
    }

}
