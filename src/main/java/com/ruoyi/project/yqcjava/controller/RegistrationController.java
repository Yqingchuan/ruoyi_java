package com.ruoyi.project.yqcjava.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.yqcjava.domain.Inspection;
import com.ruoyi.project.yqcjava.domain.Registration;
import com.ruoyi.project.yqcjava.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/reg")
public class RegistrationController extends BaseController {

    @Autowired
    private RegistrationService  registrationService;



    //查询挂号费用
    @PreAuthorize("@ss.hasPermi('system:reg:list')")
    @GetMapping("/list")
    public TableDataInfo list(Registration registration){
        startPage();
        List<Registration> list = registrationService.selctAll(registration);
        return getDataTable(list);
    }


    //新增挂号
    @PreAuthorize("@ss.hasPermi('system:reg:add')")
    @Log(title = "挂号管理", businessType = BusinessType.INSERT)
    @PostMapping("/list")
    public AjaxResult add(@RequestBody Registration registration)
    {

        registration.setCreateBy(SecurityUtils.getUsername());
        return toAjax(registrationService.inRegistration(registration));
    }

    /**
     * 根据挂号ID获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:reg:query')")
    @GetMapping(value = "/{registrationId}")
    public AjaxResult getInfo(@PathVariable Long registrationId)
    {
        return AjaxResult.success(registrationService.selectRegById(registrationId));
    }


    //修改检查费用信息
    @PreAuthorize("@ss.hasPermi('system:reg:edit')")
    @Log(title = "挂号管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit( @RequestBody Registration registration)
    {

        registration.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(registrationService.upReg(registration));
    }

    //删除挂号费用信息
    @PreAuthorize("@ss.hasPermi('system:reg:remove')")
    @Log(title = "检查管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{registrationId}")
    public AjaxResult remove(@PathVariable Long[] registrationId)
    {
        return toAjax(registrationService.deleteById(registrationId));
    }

}
