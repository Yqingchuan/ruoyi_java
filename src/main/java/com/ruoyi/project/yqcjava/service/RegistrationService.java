package com.ruoyi.project.yqcjava.service;

import com.ruoyi.project.yqcjava.domain.Registration;

import java.util.List;

public interface RegistrationService {

    //挂号费用查询
    List<Registration>   selctAll(Registration  registration);

    //挂号新增
    int   inRegistration(Registration registration);

    //根据id查询   回显
    Registration   selectRegById(Long registrationId );

    //修改挂号信息
    int  upReg(Registration registration);

    //删除挂号信息
    int  deleteById(Long[] registrationId);
}
