package com.ruoyi.project.yqcjava.service.impl;


import com.ruoyi.project.yqcjava.domain.Inspection;
import com.ruoyi.project.yqcjava.domain.Section;
import com.ruoyi.project.yqcjava.mapper.InspectionMapper;
import com.ruoyi.project.yqcjava.service.InspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InspectionServiceImpl implements InspectionService {

    @Autowired
    private InspectionMapper  inspectionMapper;


    //查询检查列表
    @Override
    public List<Inspection> selectInspection(Inspection inspection) {
        return inspectionMapper.selectInspec(inspection);
    }

    //新增检查
    @Override
    public int insection(Inspection inspection) {
        return inspectionMapper.insertInsperc(inspection);
    }

    //根据Id查询检查的信息  进行数据的回显
    @Override
    public Inspection selectById(Long projectId) {
        return inspectionMapper.selectInspectionById(projectId);
    }

    //修改检查费用
    @Override
    public int updateInspec(Inspection inspection) {
        return inspectionMapper.updateInspection(inspection);
    }

    //删除检查费用信息
    @Override
    public int deleInspec(Long[] projectId) {
        return inspectionMapper.deleteInspection(projectId);
    }
}
