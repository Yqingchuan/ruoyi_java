package com.ruoyi.project.yqcjava.service;

import com.ruoyi.project.yqcjava.domain.Inspection;
import com.ruoyi.project.yqcjava.domain.Section;


import java.util.List;

public interface InspectionService {

    //查询菜单
    List<Inspection> selectInspection(Inspection  inspection);


    //新增检查
    int   insection(Inspection inspection);

    //根据ID查询数据信息  回显
    Inspection  selectById(Long  projectId);

    //修改检查页面
    int   updateInspec(Inspection  inspection);

    //删除检查
    int  deleInspec(Long[] projectId);

}
