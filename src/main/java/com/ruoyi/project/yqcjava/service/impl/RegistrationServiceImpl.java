package com.ruoyi.project.yqcjava.service.impl;

import com.ruoyi.project.yqcjava.domain.Registration;
import com.ruoyi.project.yqcjava.mapper.RegistrationMapper;
import com.ruoyi.project.yqcjava.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private RegistrationMapper  registrationMapper;


    //挂号费用查询
    @Override
    public List<Registration> selctAll(Registration registration) {
        return registrationMapper.selectRegist(registration);
    }
    //新增挂号
    @Override
    public int inRegistration(Registration registration) {
        return registrationMapper.insertReg(registration);
    }

    //根据Id查询  回显
    @Override
    public Registration selectRegById(Long registrationId) {
        return registrationMapper.selctById(registrationId);
    }

    //修改挂号信息
    @Override
    public int upReg(Registration registration) {
        return registrationMapper.updeteReg(registration);
    }

    //删除
    @Override
    public int deleteById(Long[] registrationId) {
        return registrationMapper.deleteRegById(registrationId);
    }
}
