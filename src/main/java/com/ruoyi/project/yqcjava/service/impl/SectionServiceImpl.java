package com.ruoyi.project.yqcjava.service.impl;

import com.ruoyi.project.yqcjava.domain.Section;
import com.ruoyi.project.yqcjava.mapper.SectionMapper;
import com.ruoyi.project.yqcjava.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {

    @Autowired
    private SectionMapper sectionMapper;


    //查询科室
    @Override
    public List<Section> selectSection(Section section) {
        return sectionMapper.selecSection(section);
    }

    //新增科室
    @Override
    public int insection(Section section) {
        return sectionMapper.insertSection(section);
    }

    //根据科室ID查询科室信息（修改的回显）
    @Override
    public Section selectById(Long secId) {
        return sectionMapper.selectSectionId(secId);
    }

    //修改科室信息
    @Override
    public int upSection(Section section) {
        return sectionMapper.updatesection(section);
    }

    //删除科室信息
    @Override
    public int deleteSectionById(Long[] secIds) {
        return sectionMapper.deleteSection(secIds);
    }
}
