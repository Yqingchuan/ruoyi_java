package com.ruoyi.project.yqcjava.service;

import com.ruoyi.project.yqcjava.domain.Section;

import java.util.List;

public interface SectionService {

    //查询菜单
    List<Section>   selectSection(Section  section);

    //新增菜单
    int   insection(Section section);

    //根据科室id查询数据（修改的回显）
    Section  selectById(Long  secId);

    //修改科室信息
    int  upSection(Section section);

    //删除科室信息
    int   deleteSectionById(Long[]  secIds);
}
