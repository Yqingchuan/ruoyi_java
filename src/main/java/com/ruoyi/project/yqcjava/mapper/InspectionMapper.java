package com.ruoyi.project.yqcjava.mapper;

import com.ruoyi.project.yqcjava.domain.Inspection;
import com.ruoyi.project.yqcjava.domain.Section;
import io.swagger.models.auth.In;


import java.util.List;

public interface InspectionMapper {

    //查询检查费用集合
    List<Inspection> selectInspec(Inspection  inspection);

    //新增检查
    int   insertInsperc(Inspection inspection);

    //通过检查id查询检查信息（修改的回显）
    Inspection  selectInspectionById(Long  projectId);

    //修改检查
    int  updateInspection(Inspection inspection);

    //删除检查
    int   deleteInspection(Long[] projectId);
}
