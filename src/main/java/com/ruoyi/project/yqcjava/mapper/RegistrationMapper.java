package com.ruoyi.project.yqcjava.mapper;

import com.ruoyi.project.yqcjava.domain.Registration;

import java.util.List;

public interface RegistrationMapper {

    //查询挂号费用列表
    List<Registration>  selectRegist(Registration  registration);

    //新增挂号
    int  insertReg(Registration registration);

    //根据挂号ID查询   修改的回显
    Registration  selctById(Long  registrationId);

    //修改挂号
    int  updeteReg(Registration  registration);

    //删除挂号
    int  deleteRegById(Long[] registrationId);
}
