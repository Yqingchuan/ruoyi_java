package com.ruoyi.project.yqcjava.mapper;

import com.ruoyi.project.yqcjava.domain.Section;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SectionMapper {

    //查询科室集合
    List<Section>  selecSection(Section  section);

    //新增科室
    int   insertSection(Section  section);

    //通过科室id查询科室信息（修改的回显）
    Section   selectSectionId(Long  secId);

    //修改科室信息
    int  updatesection(Section section);

    //删除科室
    int deleteSection(Long[] roleIds);
}
