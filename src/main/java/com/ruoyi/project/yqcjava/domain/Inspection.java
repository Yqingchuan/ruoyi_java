package com.ruoyi.project.yqcjava.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Inspection extends BaseEntity {

    //项目费用ID
    private int  projectId;
    //项目名称
    private  String  projectName;
    //关键字
    private  String  projectKeyword;
    //项目单价
    private   Integer  projectPrice;
    //项目成本
    private   Integer  projectItem;
    //单位
    private  String  projectUnit;
    //类别
    private  String  projectClasses;
    //状态
    private String   status;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createTime;
    //备注
    private String  remark;

    @Override
    public String toString() {
        return "Inspection{" +
                "projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", projectKeyword='" + projectKeyword + '\'' +
                ", projectPrice=" + projectPrice +
                ", projectItem=" + projectItem +
                ", projectUnit='" + projectUnit + '\'' +
                ", projectClasses='" + projectClasses + '\'' +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inspection that = (Inspection) o;
        return projectId == that.projectId &&
                Double.compare(that.projectPrice, projectPrice) == 0 &&
                Double.compare(that.projectItem, projectItem) == 0 &&
                Objects.equals(projectName, that.projectName) &&
                Objects.equals(projectKeyword, that.projectKeyword) &&
                Objects.equals(projectUnit, that.projectUnit) &&
                Objects.equals(projectClasses, that.projectClasses) &&
                Objects.equals(status, that.status) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, projectName, projectKeyword, projectPrice, projectItem, projectUnit, projectClasses, status, createTime, remark);
    }
}
