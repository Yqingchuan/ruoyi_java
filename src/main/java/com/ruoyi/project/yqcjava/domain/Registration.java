package com.ruoyi.project.yqcjava.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Registration extends BaseEntity {
    //挂号费用ID
    private int  registrationId;
    //挂号名称
    private  String  registrationName;
    //挂号费
    private  int  registrationFee;
    //状态
    private String   status;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createTime;
    //备注
    private String  remark;

    @Override
    public String toString() {
        return "Registration{" +
                "registrationId=" + registrationId +
                ", registrationName='" + registrationName + '\'' +
                ", registrationFee=" + registrationFee +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registration that = (Registration) o;
        return registrationId == that.registrationId &&
                registrationFee == that.registrationFee &&
                Objects.equals(registrationName, that.registrationName) &&
                Objects.equals(status, that.status) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationId, registrationName, registrationFee, status, createTime, remark);
    }
}
