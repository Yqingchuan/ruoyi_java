package com.ruoyi.project.yqcjava.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Section extends BaseEntity {

    private Integer secId;
    private  String  secName;
    private  String  secComdion;
    private Integer  secCount;
    private String  secPrincipal;
    private String   secPhone;
    private String   status;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createTime;
    private String  remark;


    @Override
    public String toString() {
        return "Section{" +
                "secId=" + secId +
                ", secName='" + secName + '\'' +
                ", secComdion='" + secComdion + '\'' +
                ", secCount=" + secCount +
                ", secPrincipal='" + secPrincipal + '\'' +
                ", secPhone='" + secPhone + '\'' +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return Objects.equals(secId, section.secId) &&
                Objects.equals(secName, section.secName) &&
                Objects.equals(secComdion, section.secComdion) &&
                Objects.equals(secCount, section.secCount) &&
                Objects.equals(secPrincipal, section.secPrincipal) &&
                Objects.equals(secPhone, section.secPhone) &&
                Objects.equals(status, section.status) &&
                Objects.equals(createTime, section.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(secId, secName, secComdion, secCount, secPrincipal, secPhone, status, createTime);
    }
}
