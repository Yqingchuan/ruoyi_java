package com.ruoyi.project.drugs.supplier.mapper;

import com.ruoyi.project.drugs.supplier.domain.DruSupport;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DruSupMapper {
    //查询供应商信息
    List<DruSupport> selectSupAll(DruSupport druSupport);
    //新增供应商信息
    @Insert("insert into drugs_support(support_name,support_boss,support_phone,support_card,support_address,support_status,create_time) values(#{name},#{boss},#{phone},#{card},#{address},#{status},#{createTime})")
    int addSupOne(DruSupport druSupport);
    //主键查询
    DruSupport seletById(Long id);
    //修改供应商
    int updateSup(DruSupport druSupport);
    //删除供应商
    int deleteSup(@Param("id") Long[] id);
}
