package com.ruoyi.project.drugs.supplier.service.impl;

import com.ruoyi.project.drugs.drug.mapper.DrugMapper;
import com.ruoyi.project.drugs.supplier.domain.DruSupport;
import com.ruoyi.project.drugs.supplier.mapper.DruSupMapper;
import com.ruoyi.project.drugs.supplier.service.IDruSupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DruSupServiceImpl implements IDruSupService {

    @Autowired
    private DruSupMapper druSupMapper;

    @Override
    public List<DruSupport> selectDruSupAll(DruSupport druSupport) {
        List<DruSupport> druSupports = druSupMapper.selectSupAll(druSupport);
        return druSupports;
    }

    @Override
    public int addDruSupOne(DruSupport druSupport) {
        druSupport.setCreateTime(new Date());
        int i = druSupMapper.addSupOne(druSupport);
        return i;
    }

    @Override
    public DruSupport selectById(Long id) {
        DruSupport druSupport = druSupMapper.seletById(id);
        return druSupport;
    }

    @Override
    public int updateSupById(DruSupport druSupport) {
        int i = druSupMapper.updateSup(druSupport);
        return i;
    }

    @Override
    public int deleteSupById(Long[] id) {
        return druSupMapper.deleteSup(id);
    }
}
