package com.ruoyi.project.drugs.supplier.service;

import com.ruoyi.project.drugs.supplier.domain.DruSupport;

import java.util.List;

public interface IDruSupService {
    //查询供应商信息
    List<DruSupport> selectDruSupAll(DruSupport druSupport);
    //添加供应商信息
    int addDruSupOne(DruSupport druSupport);
    //主键查询
    DruSupport selectById(Long id);
    //修改供应商
    int updateSupById(DruSupport druSupport);
    //删除供应商
    int deleteSupById(Long[] id);
}
