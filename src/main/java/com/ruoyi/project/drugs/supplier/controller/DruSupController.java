package com.ruoyi.project.drugs.supplier.controller;


import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.drugs.supplier.domain.DruSupport;
import com.ruoyi.project.drugs.supplier.service.IDruSupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drugs/supplier")
public class DruSupController extends BaseController {
    @Autowired
    private IDruSupService iDruSupService;


    //查询供应商
    @PreAuthorize("@ss.hasAnyPermi('drugs:supplier:list')")
    @GetMapping("/list")
    public TableDataInfo list(DruSupport druSupport){
        startPage();
        List<DruSupport> druSupports = iDruSupService.selectDruSupAll(druSupport);
        return getDataTable(druSupports);
    }

    //添加供应商
    @PreAuthorize("@ss.hasAnyPermi('drugs:supplier:add')")
    @PostMapping
    public AjaxResult addSup(@RequestBody DruSupport druSupport){
        druSupport.setCreateBy(SecurityUtils.getUsername());
        System.out.println(druSupport);
        int i = iDruSupService.addDruSupOne(druSupport);
        return toAjax(i);
    }

    //修改回显
    @GetMapping("/{id}")
    @PreAuthorize("@ss.hasAnyPermi('drugs:supplier:query')")
    public AjaxResult getById(@PathVariable Long id){
        DruSupport druSupport = iDruSupService.selectById(id);
        return AjaxResult.success(druSupport);
    }

    //修改供应商
    @PutMapping
    @PreAuthorize("@ss.hasAnyPermi('drugs:supplier:edit')")
    public AjaxResult editSup(@RequestBody DruSupport druSupport){
        druSupport.setUpdateBy(SecurityUtils.getUsername());
        int i = iDruSupService.updateSupById(druSupport);
        return toAjax(i);
    }
    //删除供应商
    @DeleteMapping("/{id}")
    @PreAuthorize("@ss.hasAnyPermi('drugs:supplier:remove')")
    public AjaxResult removeSup(@PathVariable Long[] id){
        int i = iDruSupService.deleteSupById(id);
        return toAjax(i);
    }
}
