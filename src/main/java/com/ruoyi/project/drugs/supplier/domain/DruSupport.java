package com.ruoyi.project.drugs.supplier.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DruSupport extends BaseEntity {
    private int id;
    private String name;
    private String boss;//供应商联系人
    private String phone;
    private String card;
    private String address;
    private String status;//状态
}
