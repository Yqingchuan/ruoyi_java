package com.ruoyi.project.drugs.drug.service.impl;

import com.ruoyi.project.drugs.drug.domain.Drug;
import com.ruoyi.project.drugs.drug.domain.Producer;
import com.ruoyi.project.drugs.drug.mapper.DrugMapper;
import com.ruoyi.project.drugs.drug.service.IDrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DrugServiceImpl implements IDrugService {
    @Autowired
    private DrugMapper drugMapper;

    @Override
    public List<Drug> selectDrugAll(Drug drug) {
        return drugMapper.selectAll(drug);
    }

    @Override
    public List<Producer> selectPro() {
        List<Producer> producers=drugMapper.selectPro();
        return producers;
    }
}
