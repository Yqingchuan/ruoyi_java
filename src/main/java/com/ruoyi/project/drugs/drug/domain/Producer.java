package com.ruoyi.project.drugs.drug.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Producer extends BaseEntity {
    private int proid;
    private String proname;
    private String procode;
    private String proboss;
    private String prophone;
    private String prokeywords;
    private String status;
}
