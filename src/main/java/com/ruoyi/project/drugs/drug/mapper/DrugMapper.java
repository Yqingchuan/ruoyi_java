package com.ruoyi.project.drugs.drug.mapper;

import com.ruoyi.project.drugs.drug.domain.Drug;
import com.ruoyi.project.drugs.drug.domain.Producer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DrugMapper {

    List<Drug> selectAll(Drug drug);

    List<Producer> selectPro();
}
