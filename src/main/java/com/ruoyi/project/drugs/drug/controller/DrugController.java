package com.ruoyi.project.drugs.drug.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.drugs.drug.domain.Drug;
import com.ruoyi.project.drugs.drug.domain.Producer;
import com.ruoyi.project.drugs.drug.service.IDrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/drugs/drug")
public class DrugController extends BaseController {
    @Autowired
    private IDrugService iDrugService;
    @PreAuthorize("@ss.hasAnyPermi('drugs:drug:list')")
    @GetMapping("/list")
    public TableDataInfo list(Drug drug){
        startPage();
        List<Drug> drugs=iDrugService.selectDrugAll(drug);
        return getDataTable(drugs);
    }
    @PreAuthorize("@ss.hasAnyPermi('drugs:drug:list')")
    @GetMapping("/list2")
    public AjaxResult list2(){
        List<Producer> producers = iDrugService.selectPro();
        return AjaxResult.success(producers);
    }
}


