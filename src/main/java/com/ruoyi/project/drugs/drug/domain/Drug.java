package com.ruoyi.project.drugs.drug.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Drug extends BaseEntity {
    private int id;
    private String name;
    private String code;
    private int producerid;
    private String type;
    private String otcType;
    private String unit;
    private int otcPrice;
    private int number;
    private int numberMin;
    private int convert;
    private String status;
    private Producer producer;
}
