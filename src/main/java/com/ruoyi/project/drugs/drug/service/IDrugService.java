package com.ruoyi.project.drugs.drug.service;

import com.ruoyi.project.drugs.drug.domain.Drug;
import com.ruoyi.project.drugs.drug.domain.Producer;

import java.util.List;

public interface IDrugService {
    List<Drug> selectDrugAll(Drug drug);

    List<Producer>selectPro();
}
