package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

//患者表
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisPatientbank extends BaseEntity {
    private Integer s_id;
    private String s_name;
    private String s_phone;
    private String s_idnumber;
    private Date s_birth;
    private Integer s_age;
    private String s_sex;
    private String s_status;
    private String s_address;
    private String s_allergy;
    private Date s_date;
    private String s_type;
    private String s_iscontagion;
    private String s_complaint  ;
    private String s_diagnostic;
    private String s_advice;
    private String s_remark;
    private String s_registration;
    private String s_create_by;
    private java.util.Date s_create_time;
    private String remark;
}
