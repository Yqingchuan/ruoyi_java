package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

//保存病例表
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisSavecase extends BaseEntity {
    private Integer s_id;
    private Date s_date;
    private String s_type;
    private String s_infect;
    private String s_chiefcomplaint;
    private String s_message;
    private String s_propose;
    private String s_remarks;
    private String s_registration_form_id;
    private String s_case_number;
}
