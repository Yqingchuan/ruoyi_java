package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;

//添加检查处方表

@NoArgsConstructor
public class VisSavePrescription extends BaseEntity {
    private Integer a_id;
    private String a_name;
    private String a_company;
    private double a_price;
    private double a_money;
    private String a_keyword;
    private String a_beizhu;
    private String a_status;
    private String a_create_by;
    private Date a_create_time;
    private String a_remark;
    private Integer s_id;

    public VisSavePrescription(int a_id, String a_name, String a_company, double a_price, double a_money, String a_keyword, String a_beizhu, String a_status, String a_create_by, Date a_create_time, String a_remark, int s_id) {
        this.a_id = a_id;
        this.a_name = a_name;
        this.a_company = a_company;
        this.a_price = a_price;
        this.a_money = a_money;
        this.a_keyword = a_keyword;
        this.a_beizhu = a_beizhu;
        this.a_status = a_status;
        this.a_create_by = a_create_by;
        this.a_create_time = a_create_time;
        this.a_remark = a_remark;
        this.s_id = s_id;
    }

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public String getA_company() {
        return a_company;
    }

    public void setA_company(String a_company) {
        this.a_company = a_company;
    }

    public double getA_price() {
        return a_price;
    }

    public void setA_price(double a_price) {
        this.a_price = a_price;
    }

    public double getA_money() {
        return a_money;
    }

    public void setA_money(double a_money) {
        this.a_money = a_money;
    }

    public String getA_keyword() {
        return a_keyword;
    }

    public void setA_keyword(String a_keyword) {
        this.a_keyword = a_keyword;
    }

    public String getA_beizhu() {
        return a_beizhu;
    }

    public void setA_beizhu(String a_beizhu) {
        this.a_beizhu = a_beizhu;
    }

    public String getA_status() {
        return a_status;
    }

    public void setA_status(String a_status) {
        this.a_status = a_status;
    }

    public String getA_create_by() {
        return a_create_by;
    }

    public void setA_create_by(String a_create_by) {
        this.a_create_by = a_create_by;
    }

    public Date getA_create_time() {
        return a_create_time;
    }

    public void setA_create_time(Date a_create_time) {
        this.a_create_time = a_create_time;
    }

    public String getA_remark() {
        return a_remark;
    }

    public void setA_remark(String a_remark) {
        this.a_remark = a_remark;
    }

    public Integer getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisSavePrescription that = (VisSavePrescription) o;
        return a_id == that.a_id &&
                Double.compare(that.a_price, a_price) == 0 &&
                Double.compare(that.a_money, a_money) == 0 &&
                s_id == that.s_id &&
                Objects.equals(a_name, that.a_name) &&
                Objects.equals(a_company, that.a_company) &&
                Objects.equals(a_keyword, that.a_keyword) &&
                Objects.equals(a_beizhu, that.a_beizhu) &&
                Objects.equals(a_status, that.a_status) &&
                Objects.equals(a_create_by, that.a_create_by) &&
                Objects.equals(a_create_time, that.a_create_time) &&
                Objects.equals(a_remark, that.a_remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a_id, a_name, a_company, a_price, a_money, a_keyword, a_beizhu, a_status, a_create_by, a_create_time, a_remark, s_id);
    }


    @Override
    public String toString() {
        return "VisSavePrescription{" +
                "a_id=" + a_id +
                ", a_name='" + a_name + '\'' +
                ", a_company='" + a_company + '\'' +
                ", a_price=" + a_price +
                ", a_money=" + a_money +
                ", a_keyword='" + a_keyword + '\'' +
                ", a_beizhu='" + a_beizhu + '\'' +
                ", a_status='" + a_status + '\'' +
                ", a_create_by='" + a_create_by + '\'' +
                ", a_create_time=" + a_create_time +
                ", a_remark='" + a_remark + '\'' +
                ", s_id=" + s_id +
                '}';
    }
}
