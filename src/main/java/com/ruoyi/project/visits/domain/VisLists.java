package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VisLists extends BaseEntity {
    private Integer s_ids;//当前号数;流水编号
    private String s_departments;//所属科室;科室名称;挂号科室
    private String s_registration;//挂号类型
    private String s_registrationTime;//挂号时段
    private java.sql.Date s_registrationTimes;//挂号时间;就诊日期
    private String s_departmentId;//科室ID
    private Integer s_money;//挂号费用
    private String s_name;//患者姓名
    private String s_doctor;//接诊医生
    private String s_registeredState;//状态
    private Integer s_id;//患者ID
}
