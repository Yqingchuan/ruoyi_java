package com.ruoyi.project.visits.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VisWarehouse extends BaseEntity {
    private Integer s_id;//id
    private String s_idnumber;//身份证
    private String s_name;//患者姓名
    private String s_phone;//患者手机号
    private String s_sex;//患者性别
    private java.sql.Date s_birth;//出生日期
    private Integer s_age;//患者年龄
    private String s_status;//信息状态
    private String s_address;//地址
    private String createBy;//创建者
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date s_create_time;//创建时间
    private String updateBy;//更新者
    private Date updateTime;//更新时间
    private String remark;//备注
    private java.sql.Date s_date;//发病日期
    private String s_type;//接诊类型
    private String s_iscontagion;//是否传染
    private String s_complaint;//主诉
    private String s_diagnostic;//诊断信息
    private String s_advice;//医生建议
    private String s_remark;//病例备注
}
