package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

//添加药用处方表
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisDrugslist extends BaseEntity {
    private Integer d_id;
    private String d_name;
    private Integer d_stock;
    private Integer d_quantity;
    private String d_company;
    private Integer d_conversionquantity;
    private double d_price;
    private double d_money;
    private String d_status;
    private String d_beizhu;
    private String d_create_by;
    private Date d_create_time;
    private String d_remark;
    private Integer s_id;
}
