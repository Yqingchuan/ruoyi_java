package com.ruoyi.project.visits.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VisRegister extends BaseEntity {
    private Integer s_id;//id
    private String s_idnumber;//身份证
    private String s_name;//患者姓名
    private String s_phone;//患者手机号
    private String s_sex;//患者性别
    private java.sql.Date s_birth;//出生日期
    private Integer s_age;//患者年龄
    private String s_address;//地址
    private String createBy;//创建者
    private Date s_create_time;//创建时间
    private String updateBy;//更新者
    private Date updateTime;//更新时间
    private String remark;//备注
}
