package com.ruoyi.project.visits.mapper;


import com.ruoyi.project.visits.domain.VisRegister;

import java.util.List;

public interface VisRegisterMapper {
    /**
     * 根据身份证查询
     */
    VisRegister selectOne(String s_idnumber);

    /**
     * 病人添加
     */
    int insertOne(VisRegister visRegister);

}
