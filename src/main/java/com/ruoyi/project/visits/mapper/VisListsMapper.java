package com.ruoyi.project.visits.mapper;

import com.ruoyi.project.visits.domain.VisLists;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface VisListsMapper {
    /**
     * 查询
     * @param
     * @return
     */
    List<VisLists> selectAll(VisLists visLists);

    /**
     * 根据id 收费
     * @param
     * @return
     */
    int update(int s_ids);

    /**
     * 根据id 退费
     * @param
     * @return
     */
    int updated(int s_ids);
}
