package com.ruoyi.project.visits.mapper;

import com.ruoyi.project.visits.domain.VisLists;
import com.ruoyi.project.visits.domain.VisWarehouse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface VisWarehouseMapper {
    /**
     * 查询
     * @param
     * @return
     */
    List<VisWarehouse> selectAll(VisWarehouse visWarehouse);

    /**
     * 查看档案
     */
    VisWarehouse selectOne(String s_idnumber);

    /**
     * 查看就诊案例
     */
    VisWarehouse selectOned(String s_idnumber);
}
