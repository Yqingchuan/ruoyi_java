package com.ruoyi.project.visits.mapper;

import com.ruoyi.common.utils.job.JobInvokeUtil;
import com.ruoyi.project.visits.domain.VisDrugslist;
import com.ruoyi.project.visits.domain.VisPatientbank;
import com.ruoyi.project.visits.domain.VisSavePrescription;
import com.ruoyi.project.visits.domain.VisSavecase;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;


@Mapper
public interface VisitsMapper {
    //新开就诊    点击图标弹出选择挂号患者界面
    List<VisPatientbank> selectMore(Integer id);

    //在点击接诊时  首先要 根据返回的id进行添加 使用 uuid 添加 挂号单id 其次 根据id进行查询 返回结果进行数据的回显(调用selectMore)
    @Insert("insert into his_savecase(s_id,s_registration_form_id,s_create_by,s_create_time,s_remark) values(#{param1},#{param2},#{param3},#{param4},#{param5})")
    int save(int id, String uuid, String name, Date date, String remark);

    //查询挂号单id
    @Select("select * from his_savecase where s_id = #{id}")
    VisSavecase selectid(int id);

    //点击保存病例之后（进行添加检查处方和药用处方）    先做病例表和处方  再进行保存操作
    //点击添加检查处方 先弹出添加处方弹窗  用户再点击添加检查项后才进行查询所有和模糊查询
    List<VisSavePrescription> selPrescription(String keyword);


    //检查项目列表   再根据用户所选择的项目  将用户多选的项目id的数组进行传递
    List<VisSavePrescription> selPrescriptionByID(@Param("ids") int[] id);


    //点击保存病例之后（进行添加检查处方和药用处方）
    //点击添加药用处方 先弹出添加处方弹窗  用户再点击添加检查项后才进行查询所有和模糊查询
    List<VisDrugslist> selVisDrugslist(String d_name);


    //对新开就诊页面的显示再显示一个状态的页面即可
    //药用处方列表   再根据用户所选择的项目  将用户多选的项目id的数组进行传递
    List<VisDrugslist> selVisDrugslistByID(@Param("ids") int[] id);


    //点击保存病例 需要将病例编号 和 病例单  保存到病例表  并且需要将处方名用户选择的处方id保存到  中间表中
    //第一步  先保存到病例单中
    @Insert("insert into his_savecase values(#{param1.s_id},#{param1.s_date},#{param1.s_type},#{param1.s_infect},#{param1.s_chiefcomplaint},#{param1.s_message},#{param1.s_propose},#{param1.s_remarks},#{param1.s_registration_form_id},#{param1.s_case_number},#{param2},#{param3},#{param4})")
    int savecase(VisSavecase savecase, String name, Date date, String remark);

    //第二步保存到 药用处方和病例表中
    int his_savecase_drugslist(int sid, int did);

    //第三步 保存到中间表中    要把点击接诊的患者id和选择的处方 int[] 传过来
    int his_savecase_saveprescription(int sid, int aid);


}
