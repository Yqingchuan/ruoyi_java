package com.ruoyi.project.visits.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;

import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.visits.domain.VisRegister;
import com.ruoyi.project.visits.service.IVisRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/visits/register")
public class VisRegisterController extends BaseController {
    @Autowired
    private IVisRegisterService iVisRegisterService;

    /**
     * 根据身份证号码获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('visits:register:list')")
    @GetMapping("/list")
    public AjaxResult getInfo(String s_idnumber)
    {
        System.out.println(s_idnumber);
        return AjaxResult.success(iVisRegisterService.selectOne(s_idnumber));
    }

    /**
     * 病人添加
     */
    @PreAuthorize("@ss.hasPermi('visits:register:list')")
    @GetMapping("/insertOne")
    public AjaxResult insertOne(VisRegister visRegister){
        int i = iVisRegisterService.insertOne(visRegister);
        return AjaxResult.success(i);
    }
}
