package com.ruoyi.project.visits.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.visits.domain.VisWarehouse;
import com.ruoyi.project.visits.service.IVisWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/visits/warehouse")
public class VisWarehouseController  extends BaseController {

    @Autowired
    private IVisWarehouseService iVisWarehouseService;

    @PreAuthorize("@ss.hasPermi('visits:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(VisWarehouse visWarehouse)
    {
        startPage();
        List<VisWarehouse> lists = iVisWarehouseService.selectAll(visWarehouse);
        return getDataTable(lists);
    }

    /**
     * 查看档案
     */
    @PreAuthorize("@ss.hasPermi('visits:warehouse:list')")
    @GetMapping("/select")
    public AjaxResult select(String s_idnumber)
    {
        System.out.println("========================"+s_idnumber);
        return AjaxResult.success(iVisWarehouseService.selectOne(s_idnumber));
    }

    /**
     * 查看档案
     */
    @PreAuthorize("@ss.hasPermi('visits:warehouse:list')")
    @GetMapping("/selected")
    public AjaxResult selected(String s_idnumber)
    {
        System.out.println("========================"+s_idnumber);
        return AjaxResult.success(iVisWarehouseService.selectOned(s_idnumber));
    }
}
