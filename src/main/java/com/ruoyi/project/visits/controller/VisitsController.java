package com.ruoyi.project.visits.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.visits.domain.VisDrugslist;
import com.ruoyi.project.visits.domain.VisPatientbank;
import com.ruoyi.project.visits.domain.VisSavePrescription;
import com.ruoyi.project.visits.domain.VisSavecase;
import com.ruoyi.project.visits.service.VisitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;
@RestController
@ResponseBody
@RequestMapping("/vis")
public class VisitsController extends BaseController {
    @Autowired
    private VisitsService visitsService;

    //返回分页数据  使用TableDataInfo     返回非分页  AjaxResult   返回非ajax用toAjax


    //新开就诊    点击图标弹出选择挂号患者界面  医生接诊
    @GetMapping("visits:newvisits:query")
    @PreAuthorize("@ss.hasAnyPermi('visits:newvisits:query')")  //每一个都应该是这样写  下次要这样写
    @RequestMapping("/findAll/{id}")
    public AjaxResult findAll(@PathVariable Integer id){
        if(id == 0){
            id = null;
        }
        List<VisPatientbank> visPatientbanks = visitsService.selectMore(id);
        return AjaxResult.success(visPatientbanks);
    }

    //前台 点接诊  传过来 VisPatientbank 的 id

    @PostMapping("visits:newvisits:query")
    @RequestMapping("/save/{id}")
    public AjaxResult save(@PathVariable Integer id, HttpSession session){
        int save = visitsService.save(id,session);
        return toAjax(save);
    }
    //点击接诊的同时需要查询挂号单id并显示  在需要显示病例编号的时候再调用一次方法即可
    @PostMapping("visits:newvisits:query")
    @RequestMapping("/findid/{id}")
    public AjaxResult findid(@PathVariable Integer id){
        System.out.println(id);
        return AjaxResult.success(visitsService.findid(id));
    }

    //未点击保存病例（进行添加检查处方和药用处方） 查询全部和模糊查询
    //点击添加检查处方 先弹出添加处方弹窗  用户再点击添加检查项后才进行查询所有和模糊查询
    @GetMapping("visits:newvisits:saveprescription")
    @RequestMapping("/findPrescription/{keyword}")
    public AjaxResult findPrescription(@PathVariable String keyword){
        List<VisSavePrescription> vis = visitsService.selPrescription(keyword);
        return AjaxResult.success(vis);
    }


    //再添加检查处方中显示用户选择的数据      点击确定添加还需要调用一下这个方法把数据显示到新开就诊的处方栏中
    //检查项目  再根据用户所选·择的项目  将用户多选的项目id的数组进行传递   保存病例中的添加检查处方  将检查项目列表中用户选择的数据写道添加检查处方表中
    @GetMapping("visits:newvisits:saveprescription")
    @RequestMapping("/findPrescriptionByID/{ids}")
    public AjaxResult findPrescriptionByID(@PathVariable int[] ids){
        return AjaxResult.success(visitsService.selPrescriptionByID(ids));
    }

    @GetMapping("visits:newvisits:drugslist")
    @RequestMapping("/findselVisDrugslist/{name}")
    public AjaxResult findselVisDrugslist(@PathVariable String name){
        List<VisDrugslist> visDrugslists = visitsService.selVisDrugslist(name);
        return AjaxResult.success(visDrugslists);
    }


    //再添加检查处方中显示用户选择的数据      点击确定添加还需要调用一下这个方法把数据显示到新开就诊的处方栏中
    //检查项目  再根据用户所选择的项目  将用户多选的项目id的数组进行传递   保存病例中的添加检查处方  将检查项目列表中用户选择的数据写道添加检查处方表中
    @GetMapping("/findVisDrugslistByID/{idsyy}")
    public AjaxResult selVisDrugslistByID(@PathVariable int[] idsyy){
        return AjaxResult.success(visitsService.selVisDrugslistByID(idsyy));
    }


    //点击保存病例 (1) 需要将病例编号 和 病例单  保存到病例表  并且需要将处方名用户选择的处方id保存到  中间表中  还是保存到
    @PostMapping("visits:newvisits:saveprescription")
    @RequestMapping("/savebl1")
    public AjaxResult savebl1( VisSavecase visSavecase,HttpSession session){
        System.out.println(visSavecase);
        int save = visitsService.savecase1(visSavecase,session);
        return toAjax(save);
    }

    //点击保存病例 (1) 需要将病例编号 和 病例单  保存到病例表  并且需要将处方名用户选择的处方id保存到  中间表中  还是保存到
    /*@PostMapping("visits:newvisits:saveprescription")*/
    @GetMapping("/savebl2/{aid}/{did}")
    public AjaxResult savebl2(@PathVariable int[] aid, @PathVariable int[] did, HttpSession session){
        System.out.println(aid);
        System.out.println(did);;
        int save = visitsService.savecase2(aid,did,session);
        return toAjax(save);
    }


}
