package com.ruoyi.project.visits.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.visits.domain.VisLists;
import com.ruoyi.project.visits.service.IVisListsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/visits/lists")
public class VisListsController extends BaseController {

    @Autowired
    private IVisListsService iVisListsService;

    @PreAuthorize("@ss.hasPermi('visits:lists:list')")
    @GetMapping("/list")
    public TableDataInfo list(VisLists visLists)
    {
        startPage();
        List<VisLists> lists = iVisListsService.selectAll(visLists);
        return getDataTable(lists);
    }


    /**
     * 收费
     */
    @PreAuthorize("@ss.hasPermi('visits:lists:edit')")
    @GetMapping("/edit")
    public AjaxResult edit(int s_ids)
    {
        System.out.println("--------------------------"+s_ids);
        int update = iVisListsService.update(s_ids);
        return toAjax(update);
    }

    /**
     * 退费
     */
    @PreAuthorize("@ss.hasPermi('visits:lists:edit')")
    @GetMapping("/editd")
    public AjaxResult editd(int s_ids)
    {
        System.out.println("--------------------------"+s_ids);
        int update = iVisListsService.updated(s_ids);
        return toAjax(update);
    }

}
