package com.ruoyi.project.visits.service.impl;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.project.visits.domain.VisDrugslist;
import com.ruoyi.project.visits.domain.VisPatientbank;
import com.ruoyi.project.visits.domain.VisSavePrescription;
import com.ruoyi.project.visits.domain.VisSavecase;
import com.ruoyi.project.visits.mapper.VisitsMapper;
import com.ruoyi.project.visits.service.VisitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
@Transactional
public class VisitsServiceImpl implements VisitsService {
    @Autowired
    private  VisitsMapper visitsMapper;
    @Override
    public List<VisPatientbank> selectMore(Integer id) {
        return visitsMapper.selectMore(id);
    }

    @Override    //前台 点接诊  传过来 VisPatientbank 的 id
    @Transactional
    public int save(int id, HttpSession session) {
        session.setAttribute("id",id);
        String name = SecurityUtils.getUsername();
        String uuid = UUID.randomUUID().toString();
        int save = visitsMapper.save(id,uuid,name,new Date(),"这是备注");
        List<VisPatientbank> bank = visitsMapper.selectMore(id);
        if(save > 0&&bank != null){
            return 1;
        }
        return 0;
    }

    @Override
    public VisSavecase findid(int id) {
        VisSavecase savecase = visitsMapper.selectid(id);
        System.out.println(savecase);
        return savecase;
    }

    @Override
    public List<VisSavePrescription> selPrescription(String keyword) {
        return visitsMapper.selPrescription(keyword);
    }

    @Override
    public Map<String,Object> selPrescriptionByID(int[] ids) {
        int num = 0;
        List<VisSavePrescription> visSavePrescriptions = visitsMapper.selPrescriptionByID(ids);
        for (VisSavePrescription v:visSavePrescriptions) {
            num+=v.getA_price();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("rows",visSavePrescriptions);
        map.put("nums",num);
        return map;
    }

    @Override
    public List<VisDrugslist> selVisDrugslist(String name) {
        return visitsMapper.selVisDrugslist(name);
    }

    @Override
    public Map<String, Object> selVisDrugslistByID(int[] id) {
        int num = 0;
        List<VisDrugslist> visDrugslists = visitsMapper.selVisDrugslistByID(id);
        for (VisDrugslist v:visDrugslists) {
            num+=v.getD_price();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("rows",visDrugslists);
        map.put("nums",num);
        return map;
    }

    @Override
    @Transactional
    public int savecase1(VisSavecase savecase,HttpSession httpSession) {
        Integer id = (Integer) httpSession.getAttribute("id");
        int i = visitsMapper.savecase(savecase, UUID.randomUUID().toString(), new Date(), "保存病例表");
        if (i > 0){
            return 1;
        }
        return 0;
    }

    @Override
    public int savecase2(int[] ids,int[] did, HttpSession httpSession) {
        //这里可优化  定义一个变量  每一次循环就加一次  如果加到最后与 ids或 did   .length()方法相等的话  就说明正确无误

        Integer id = (Integer) httpSession.getAttribute("id");
        int id1 = 0;
        int id2 = 0;
        for (int i:ids) {
            visitsMapper.his_savecase_saveprescription(id,i);
            id1++;
        }
       for(int i:did){
           visitsMapper.his_savecase_drugslist(id, i);
           id2++;
       }

       if(id1 == ids.length&&id2 == did.length){
           return 1;
       }
        return 0;
    }

}
