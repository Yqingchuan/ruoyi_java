package com.ruoyi.project.visits.service;

import com.ruoyi.project.visits.domain.VisLists;

import java.util.List;

public interface IVisListsService {

    List<VisLists> selectAll(VisLists visLists);


    int update(int s_ids);

    int updated(int s_ids);
}
