package com.ruoyi.project.visits.service.impl;

import com.ruoyi.project.visits.domain.VisLists;
import com.ruoyi.project.visits.mapper.VisListsMapper;
import com.ruoyi.project.visits.service.IVisListsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisListsServiceImpl implements IVisListsService {

    @Autowired
    private VisListsMapper visListsMapper;

    @Override
    public List<VisLists> selectAll(VisLists visLists) {

        return visListsMapper.selectAll(visLists);
    }

    @Override
    public int update(int s_ids) {
        return visListsMapper.update(s_ids);
    }

    @Override
    public int updated(int s_ids) {
        return visListsMapper.updated(s_ids);
    }
}
