package com.ruoyi.project.visits.service;

import com.ruoyi.project.visits.domain.VisDrugslist;
import com.ruoyi.project.visits.domain.VisPatientbank;
import com.ruoyi.project.visits.domain.VisSavePrescription;
import com.ruoyi.project.visits.domain.VisSavecase;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface VisitsService {
    //新开就诊    点击图标弹出选择挂号患者界面
    List<VisPatientbank> selectMore(Integer ids);

    //在点击接诊时  首先要 根据返回的id进行添加 使用 uuid 添加 挂号单id 其次 根据id进行查询 返回结果进行数据的回显(调用selectMore)
    int save(int id, HttpSession session);

    //挂号单id查询
    VisSavecase findid(int id);

    //点击保存病例之后（进行添加检查处方和药用处方）
    //点击添加检查处方 先弹出添加处方弹窗  用户再点击添加检查项后才进行查询所有和模糊查询
    List<VisSavePrescription> selPrescription(String keyword);

    //检查项目  再根据用户所选择的项目  将用户多选的项目id的数组进行传递
    Map<String,Object> selPrescriptionByID(int[] id);

    List<VisDrugslist> selVisDrugslist(String name);

    //检查项目  再根据用户所选择的项目  将用户多选的项目id的数组进行传递
    Map<String,Object> selVisDrugslistByID(int[] id);

    //点击保存病例 需要将病例编号 和 病例单  保存到病例表  并且需要将处方名用户选择的处方id保存到  中间表中
    //第一步  先保存到病例单中
    int savecase1(VisSavecase savecase, HttpSession httpSession);
    int savecase2(int[] ids, int[] did, HttpSession httpSession);


}
