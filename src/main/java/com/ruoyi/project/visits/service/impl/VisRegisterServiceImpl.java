package com.ruoyi.project.visits.service.impl;


import com.ruoyi.project.visits.domain.VisRegister;
import com.ruoyi.project.visits.mapper.VisRegisterMapper;
import com.ruoyi.project.visits.service.IVisRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisRegisterServiceImpl implements IVisRegisterService {
    @Autowired
    private VisRegisterMapper visRegisterMapper;

    /**
     * 根据身份证查询病人信息
     * @param
     * @return
     */
    @Override
    public VisRegister selectOne(String s_idnumber) {
        VisRegister visRegister = visRegisterMapper.selectOne(s_idnumber);
        return visRegister;
    }

    /**
     * 添加病人
     * @param
     * @return
     */
    @Override
    public int insertOne(VisRegister visRegister) {
        return visRegisterMapper.insertOne(visRegister);
    }
}
