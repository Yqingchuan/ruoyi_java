package com.ruoyi.project.visits.service;

import com.ruoyi.project.visits.domain.VisWarehouse;

import java.util.List;

public interface IVisWarehouseService {
    List<VisWarehouse> selectAll(VisWarehouse visWarehouse);

    /**
     * 查看档案
     */
    VisWarehouse selectOne(String s_idnumber);

    /**
     * 查看就诊案例
     */
    VisWarehouse selectOned(String s_idnumber);
}
