package com.ruoyi.project.visits.service.impl;

import com.ruoyi.project.visits.domain.VisWarehouse;
import com.ruoyi.project.visits.mapper.VisWarehouseMapper;
import com.ruoyi.project.visits.service.IVisWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisWarehouseServiceImpl implements IVisWarehouseService {
    @Autowired
    private VisWarehouseMapper visWarehouseMapper;
    @Override
    public List<VisWarehouse> selectAll(VisWarehouse visWarehouse) {
        return visWarehouseMapper.selectAll(visWarehouse);
    }

    /**
     * 查看档案
     */
    @Override
    public VisWarehouse selectOne(String s_idnumber) {
        return visWarehouseMapper.selectOne(s_idnumber);
    }

    /**
     * 查看就诊案例
     * @param s_idnumber
     * @return
     */
    @Override
    public VisWarehouse selectOned(String s_idnumber) {
        return visWarehouseMapper.selectOned(s_idnumber);
    }
}
