package com.ruoyi.project.visits.service;


import com.ruoyi.project.visits.domain.VisRegister;

import java.util.List;

public interface IVisRegisterService {

    /**
     * 根据身份证查询病人
     * @param
     * @return
     */
    VisRegister selectOne(String s_idnumber);

    /**
     * 添加病人
     * @param
     * @return
     */
    int insertOne(VisRegister visRegister);
}
